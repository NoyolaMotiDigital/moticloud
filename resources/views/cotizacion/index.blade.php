@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>Dashboard</h1>
<style>
    .fecha-prod{
        background: #6599f7;
    padding: 6px;
    border-radius: 1rem;
    margin: 6px 0px 6px 0px;
    color: black;
    }
    .fecha-vencimiento{
        background: #85d593;
    padding: 6px;
    border-radius: 1rem;
    margin: 6px 0px 6px 0px;
    color: black;
    }
    .fecha-preprensa{
        background: #f7d865;
    padding: 6px;
    border-radius: 1rem;
    margin: 6px 0px 6px 0px;
    color: black;
    }
</style>
@stop

@section('content')
@if(Auth::user()-> rol == 'Comercial' || Auth::user()-> rol == 'Admin-comercial' || Auth::user()-> rol == 'Admin-dev')
<a href="cotizacions/create" class="btn btn-primary mt-2 mb-2">Procesar cotización</a>
@endif
@if (\Session::has('message'))
    <div class="mt-4 alert alert-danger">
        <ul>
            <li>{!! \Session::get('message') !!}</li>
        </ul>
    </div>
@endif
<table id="Cotizaciones" class="table table-dark table-hover">
    <thead>
        <tr>
            <th scope="col"># Cotización</th>
            <th scope="col"># Trabajo </th>
            <th scope="col">Creador</th>
            <th scope="col">Preprensista asignado</th>
            <th scope="col">Fecha preprensa</th>
            <th scope="col">Fecha producción</th>
            <th scope="col">Fecha vencimiento</th>
            <th scope="col">Fecha creación</th>
            <th scope="col">Estatus</th>
            <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($cotizacions as $cotizacion)
        <tr>
            <td>{{$cotizacion->no_cotizacion}}</td>
            @if ($cotizacion->no_trabajo == null)
            <td><span class="badge rounded-pill bg-secondary text-dark">sin # de trabajo</span></td>
            @else
            <td>{{$cotizacion->no_trabajo}}</td>
            @endif
            <td>{{$cotizacion->creator_user->name}}</td>
            <td>{{$cotizacion->user->name}}</td>
            <td><span class="fecha-preprensa">{{$cotizacion->fecha_preprensa}}</span></td>
            <td><span class="fecha-prod">{{$cotizacion->fecha_produccion}}</span></td>
            <td><span class="fecha-vencimiento">{{$cotizacion->fecha_vencimiento}}</span></td>
            <td>{{$cotizacion->created_at}}</td>
            @switch($cotizacion->estatus)
            @case($cotizacion->estatus == 'Preprensa')
            <td><span class="badge rounded-pill bg-warning text-dark">{{$cotizacion->estatus}}</span></td>
            @break
            @case($cotizacion->estatus == 'Listo para impresion')
            <td><span class="badge rounded-pill bg-info text-dark">{{$cotizacion->estatus}}</span></td>
            @break
            @case($cotizacion->estatus == 'Cerrado')
            <td><span class="badge rounded-pill bg-success">{{$cotizacion->estatus}}</span></td>
            @break
            @case($cotizacion->estatus == 'Cancelado')
            <td><span class="badge rounded-pill bg-danger">{{$cotizacion->estatus}}</span></td>
            @break
            @endswitch
            <td>
                <form action="{{route ('cotizacions.destroy', $cotizacion->id)}}" method="POST">
                    <a href="/cotizacions/{{$cotizacion->id}}/edit" class="btn btn-info">Editar</a>
                    @csrf
                    @method('DELETE')
                    @if (Auth::user()-> rol == 'Comercial' || Auth::user()-> rol == 'Admin-comercial' || Auth::user()-> rol == 'Admin-dev')
                    <button class="btn btn-danger">Borrar</button>
                    @endif
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@stop

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap5.min.css">
@stop

@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap5.min.js"></script>
<script>
$(document).ready(function() {
    $('#Cotizaciones').DataTable();
});
</script>
@stop