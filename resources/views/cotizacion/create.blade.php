@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>Dashboard</h1>
<style>
    #loading {
        width: 3rem;
        height: 3rem;
        border: 5px solid #f3f3f3;
        border-top: 6px solid #ff8100;
        border-radius: 100%;
        margin: auto;
        visibility: hidden;
        animation: spin 1s infinite linear;
    }

    #loading.display {
        visibility: visible;
    }

    @keyframes spin {
        from {
            transform: rotate(0deg);
        }

        to {
            transform: rotate(360deg);
        }
    }
    .fecha-prod{
        background: #6599f7;
    padding: 6px;
    border-radius: 1rem;
    margin: 6px 0px 6px 0px;
    }
    .fecha-vencimiento{
        background: #85d593;
    padding: 6px;
    border-radius: 1rem;
    margin: 6px 0px 6px 0px;
    }
    .fecha-preprensa{
        background: #f7d865;
    padding: 6px;
    border-radius: 1rem;
    margin: 6px 0px 6px 0px;
    }
</style>
@stop
@section('content')
<h2>crear</h2>
<form action="/cotizacions" method="POST">
    @csrf
    <div class="mb-3" data-toggle="tooltip" data-placement="top" title="numero de cotización en EFI">
        <label for="no_cotizacion" class="form-label">Número de cotización</label>
        <input id="no_cotizacion" name="no_cotizacion" class="form-control" tabindex="1" type="text" onchange="obtenerItems(this)" required>
    </div>
    <div>
        <label for="fecha_produccion" class="fecha-preprensa">Fecha de Preprensa</label>
        <input type="date" name="fecha_preprensa" id="fecha_preprensa" class="form-control" required>
    </div>
    <div>
        <label for="fecha_produccion" class="fecha-prod">Fecha de producción</label>
        <input type="date" name="fecha_produccion" id="fecha_produccion" class="form-control" required>
    </div>
    <div>
        <label for="fecha_produccion" class="fecha-vencimiento">Fecha de Vencimiento</label>
        <input type="date" name="fecha_vencimiento" id="fecha_vencimiento" class="form-control" required>
    </div>
    <div class="form-group">
        <label for="comentarios">Comentarios</label>
        <textarea class="form-control" name="comentarios" id="comentarios" rows="3"></textarea>
    </div>
    <div id="loading"></div>
    <div class="form-group card" id="distribucion">
    </div>
    <div class="form-group" id="artes">
    </div>
    <a href="/cotizacions" class="btn btn-secondary" tabindex="5">Cancelar</a>
    <button type="submit" class="btn btn-success" tabindex="4">Crear</button>
    @if (\Session::has('error'))
    <div class="mt-4 alert alert-danger">
        <ul>
            <li>{!! \Session::get('error') !!}</li>
        </ul>
    </div>
    @endif
</form>
@stop
<script>

    function displayLoading() {
        var loader = document.getElementById('loading');
        loader.classList.add("display");
        setTimeout(() => {
            loader.classList.remove("display");
        }, 5000);
    }

    function hideLoading() {
        var loader = document.getElementById('loading');
        loader.classList.remove("display");
    }

    function obtenerItems(id) {
        var bloque_contenedor = document.getElementById('artes');
        document.getElementById('distribucion').innerHTML = "";
        while (bloque_contenedor.firstChild) {
            bloque_contenedor.removeChild(bloque_contenedor.firstChild);
        }
        displayLoading()
        fetch("http://motidigitalsupport.ddns.net:8082/moti-api-v2/public/api/quotes/" + id.value + "/parts", {
                method: 'get',
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json())
            .catch(error => {
                alert("ha habido un problema con la conexion a EFI, intente mas tarde y contacte a soporte ");
            })
            .then(response => {
                /* si la respuesta es un array entra a esta condicional */
                if (Array.isArray(response.response) == true) {
                    bloque_contenedor.innerHTML = '<span class="ml-2">Items de la orden</span>'
                    for (var i = 0; i < Object.keys(response.response).length; i++) {
                        A_insertar = document.createElement('div');
                        A_insertar.innerHTML = '<div class="m-2"><a href="imagen/'+response.customer+'-Originales-' + id.value.replace(/\s/g, '') + '-' + [i + 1] + '" target="blank" class="badge badge-pill badge-success">Cargar Artes originales <i class="fas fa-upload"></i></a>' + response.response[i].description + '  <div>';
                        bloque_contenedor.appendChild(A_insertar);
                        bloque_contenedor.setAttribute("class", "card");
                    }
                    A_insertar = document.createElement('div');
                    A_insertar.innerHTML = '<span class="ml-2">Archivo de distribución</span><div class="m-2"><a href="imagen/'+response.customer+'-Archivo-' + id.value.replace(/\s/g, '') + '-' + 'distribucion"  target="blank" class="badge badge-pill badge-info">Cargar archivo de distribución<i class="fas fa-upload"></i></a><div>';
                    document.getElementById('distribucion').appendChild(A_insertar);
                }
                /* si la respuesta es un objeto */
                else {
                    A_insertar = document.createElement('div');
                    A_insertar.innerHTML = '<div class="m-2"><a href="imagen/'+response.customer+'-Originales-' + id.value.replace(/\s/g, '') + '-' + 1 + '" target="blank" class="badge badge-pill badge-success">Cargar Artes originales <i class="fas fa-upload"></i></a>' + response.response.description + '  <div>';
                    bloque_contenedor.appendChild(A_insertar);
                    A_insertar = document.createElement('div');
                    A_insertar.innerHTML = '<span class="ml-2">Archivo de distribución</span><div class="m-2"><a href="imagen/'+response.customer+'-Archivo-' + id.value.replace(/\s/g, '') + '-' + 'distribucion"  target="blank" class="badge badge-pill badge-info">Cargar archivo de distribución<i class="fas fa-upload"></i></a><div>';
                    document.getElementById('distribucion').appendChild(A_insertar);
                }
                hideLoading()
            });
    
    }
</script>