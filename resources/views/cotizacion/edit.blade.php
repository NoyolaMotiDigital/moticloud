@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<style>
    .toggle-switch input {
        opacity: 0
    }

    .toggle-switch label {
        width: 60px;
        height: 30px;
        background: #ccc;
        border-radius: 50px;
        display: inline-block;
        position: relative;
    }

    .toggle-switch label:after {
        content: '';
        display: block;
        width: 28px;
        height: 28px;
        background: #fff;
        border-radius: 50%;
        position: absolute;
        top: 1px;
        left: 1px;
        transition: left .3s ease-out;
    }

    .toggle-switch input[id="togle-swith"]:checked+label:after {
        left: 31px
    }

    .toggle-switch input[id="togle-swith"]:checked+label {
        background: #3497db;
    }

    #loading {
        width: 3rem;
        height: 3rem;
        border: 5px solid #f3f3f3;
        border-top: 6px solid #ff8100;
        border-radius: 100%;
        margin: auto;
        visibility: hidden;
        animation: spin 1s infinite linear;
    }

    #loading.display {
        visibility: visible;
    }

    @keyframes spin {
        from {
            transform: rotate(0deg);
        }

        to {
            transform: rotate(360deg);
        }
    }
</style>
<h1>Dashboard</h1>
@stop
@section('content')
<h2> Editar registros</h2>
<form action="/cotizacions/{{$cotizacion->id}}" method="POST">
    @csrf
    @method ('PUT')
    <div class="mb-3" data-toggle="tooltip" data-placement="top" title="numero de cotización en EFI">
        <label for="no_cotizacion" class="form-label">Número de cotización</label>
        <input id="no_cotizacion" name="no_cotizacion" class="form-control" tabindex="1" type="text" value="{{$cotizacion->no_cotizacion}}" title="{{$cotizacion->no_cotizacion}}" onkeyup="NoEscribir(this)" readonly>
    </div>
    @if (Auth::user()-> rol == 'Planer')
    <div class="mb-3" data-toggle="tooltip" data-placement="top" title="numero de trabajo en EFI">
        <label for="no_trabajo" class="form-label">Número de trabajo</label>
        <input id="no_trabajo" name="no_trabajo" class="form-control" tabindex="1" type="text" value="{{$cotizacion->no_trabajo}}">
    </div>
    @endif
    @if (Auth::user()-> rol == 'Admin-preprensa')
    <div class="mb-3" data-toggle="tooltip" data-placement="top" title="{{$cotizacion->user->name}}">
        <label for="empleado" class="form-label">Asignar Preprensista</label>
        <select name="empleado" id="empleado">
            @foreach($usuarios as $usuario)
            @if ($usuario->rol == 'Preprensista' || $usuario->rol == 'Admin-preprensa')
            @if ($usuario->id == $cotizacion->user->id)
            <option value="{{ $usuario->id }}" selected>
                {{ $usuario->name }}
            </option>
            @else
            <option value="{{ $usuario->id }}">
                {{ $usuario->name }}
            </option>
            @endif
            @endif
            @endforeach
        </select>
    </div>
    @endif
    <div class="form-group">
        @if (Auth::user()-> rol == 'Admin-comercial' || Auth::user()-> rol == 'Comercial')
        <label for="comentarios">Comentarios</label>
        <textarea class="form-control" name="comentarios" id="comentarios" rows="3">{{$cotizacion->comentarios}}</textarea>
        @else
        <label for="comentarios">Comentarios</label>
        <textarea class="form-control" name="comentarios" id="comentarios" rows="3" title="{{$cotizacion->comentarios}}" onkeyup="NoEscribir(this)" readonly>{{$cotizacion->comentarios}}</textarea>
        @endif
    </div>
    <div class="form-group card" id="distribucion">
    </div>
    <div id="loading"></div>
    <div class="form-group" id="artes">
    </div>
    <a href="/cotizacions" class="btn btn-secondary" tabindex="5">Cancelar</a>
    <button type="submit" class="btn btn-success" tabindex="4">Confirmar</button>
    </div>
    @if (Auth:: user()-> rol == 'Preprensista' || Auth:: user()-> rol == 'Admin-preprensa' || Auth:: user()-> rol == 'Planer')
</form>
@if (Auth:: user()-> rol == 'Preprensista' || Auth:: user()-> rol == 'Admin-preprensa')
<?php $ruta = "Cerrado";
$texto = "Marcar como Cerrado" ?>
@elseif(Auth:: user()-> rol == 'Planer')
<?php $ruta = "Cerrado";
$texto = "Marcar como Cerrado" ?>
@endif
<form action="{{route ('cotizacion.status', [$ruta, $cotizacion->id])}}" method="POST">
    @csrf
    <button class="btn btn-danger" onclick="return confirm('Estas completamente seguro de cambiar a {{$ruta}} ?');">{{$texto}}</button>
</form>
@endif
@endsection
<script>
    window.onload = function() {
        ObtenerItems();
    }

    function NoEscribir(e) {
        e.value = e.title;
        e.readOnly = true;
    }
    function displayLoading() {
        var loader = document.getElementById('loading');
        loader.classList.add("display");
        setTimeout(() => {
            loader.classList.remove("display");
        }, 5000);
    }
    function hideLoading() {
        var loader = document.getElementById('loading');
        loader.classList.remove("display");
    }
    async function ObtenerItems() {
        var id = document.getElementById('no_cotizacion');
        var bloque_contenedor = document.getElementById('artes');
        var respuestas = [];
        while (bloque_contenedor.firstChild) {
            bloque_contenedor.removeChild(bloque_contenedor.firstChild);
        }
        displayLoading()
        fetch("http://motidigitalsupport.ddns.net:8082/moti-api-v2/public/api/quotes/" + id.value + "/parts", {
                method: 'get',
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json())
            .catch(error => {
                alert("ha habido un problema con la conexion a EFI, intente mas tarde y contacte a soporte ");
            })
            .then(response => {
                /* si la respuesta es un array entra a esta condicional */
                if (Array.isArray(response.response) == true) {

                    for (var i = 0; i < Object.keys(response.response).length; i++) {
                        let descripcion = response.response[i].description;
                        let id_trim = id.value.replace(/\s/g, '') + '-' + [i + 1];
                        A_insertar = document.createElement('div');
                        "@if(Auth::user()-> rol == 'Comercial' || Auth::user()-> rol == 'Admin-comercial' || Auth::user()-> rol == 'Admin-dev')" /* dependiendo del rol crea el boton para cada uno */
                        let textoBoton = "Cargar Artes originales";
                        let clase = "success";
                        let url = "../imagen/"+response.customer+"-Originales-" + id_trim + "";
                        "@elseif(Auth::user()-> rol == 'Preprensista' || Auth::user()-> rol == 'Admin-preprensa')"
                        let textoBoton = "Cargar Preprensa Terminada";
                        let clase = "warning";
                        let url = "../imagen/"+response.customer+"-Preprensa-" + id_trim + "";
                        "@endif"
                        "@if(Auth::user()-> rol == 'Comercial' || Auth::user()-> rol == 'Admin-comercial' || Auth::user()-> rol == 'Preprensista' || Auth::user()-> rol == 'Admin-preprensa' ||  Auth::user()-> rol == 'Admin-dev')"
                        const downloadCheck = fetch("../download/imagen/"+response.customer+"-Originales-" + id_trim)
                            .then(function(response) {
                                if (response.ok) {
                                    return true;
                                } else {
                                    return false;
                                }
                            })
                        const printAddress = async () => {
                            const a = await downloadCheck;
                            if (a == true) {
                                A_insertar = document.createElement('div');
                                A_insertar.innerHTML =
                                    '<div class="m-2"><a href="' + url + '" target="blank" class="badge badge-pill badge-' +
                                    clase + '">' + textoBoton + ' <i class="fas fa-upload"></i></a>' +
                                    descripcion +
                                    '  <a href="../download/imagen/'+response.customer+'-Originales-' + id_trim +
                                    '" class="badge badge-pill badge-primary">Descargar Artes originales<i class="fas fa-download"></i></a> <div> ';
                                bloque_contenedor.appendChild(A_insertar);
                            } else {
                                A_insertar = document.createElement('div');
                                A_insertar.innerHTML =
                                    '<div class="m-2"><a href="' + url + '" target="blank" class="badge badge-pill badge-' +
                                    clase + '">' + textoBoton + ' <i class="fas fa-upload"></i></a>' +
                                    descripcion +
                                    '';
                                bloque_contenedor.appendChild(A_insertar);
                            }
                        };
                        printAddress();
                        "@endif"
                    }
                    A_insertar = document.createElement('div');
                    A_insertar.innerHTML = '<span class="ml-2">Archivo de distribución</span><div class="m-2"><a href="../imagen/'+response.customer+'-Archivo-' + id.value.replace(/\s/g, '') + '-' + 'distribucion"  target="blank" class="badge badge-pill badge-info">Cargar archivo de distribución<i class="fas fa-upload"></i></a><div>';
                    document.getElementById('distribucion').appendChild(A_insertar);
                }
                /* si la respuesta es un objeto  es decir si la cotizacion solo tiene un item*/
                else {
                    let id_trim = id.value.replace(/\s/g, '') + '-' + 1;
                    let descripcion = response.response.description
                    /* dependiendo del rol crea el boton para cada uno */
                    "@if(Auth::user()-> rol == 'Comercial' || Auth::user()-> rol == 'Admin-comercial' || Auth::user()-> rol == 'Admin-dev')"
                    let textoBoton = "Cargar Artes originales";
                    let clase = "success";
                    let url = "../imagen/"+response.customer+"-Originales-" + id_trim + "";
                    "@elseif(Auth::user()-> rol == 'Preprensista' || Auth::user()-> rol == 'Admin-preprensa')"
                    let textoBoton = "Cargar Preprensa Terminada";
                    let clase = "warning";
                    let url = "../imagen/"+response.customer+"-Preprensa-" + id_trim + "";
                    "@endif"
                    "@if(Auth::user()-> rol == 'Comercial' || Auth::user()-> rol == 'Admin-comercial' || Auth::user()-> rol == 'Preprensista' || Auth::user()-> rol == 'Admin-preprensa' ||  Auth::user()-> rol == 'Admin-dev')"
                    fetch("../download/imagen/"+response.customer+"-Originales-" + id_trim) /* hace la descarga de los archivos metodo en ImgenController  */
                        .then(function(response) {
                            if (response.ok) {
                                /* si la respuesta es positiva es decir la url permite descargar  */
                                A_insertar = document.createElement('div');
                                A_insertar.innerHTML = /* se crea el item con sus dos botones dependiendo del rol y con su boton para descargar */
                                    '<div class="m-2"><a href="' + url +
                                    '" target="blank" " class="badge badge-pill badge-success">' + textoBoton + '<i class="fas fa-upload"></i></a>' +
                                    descripcion + '  <a href="../download/imagen/Originales-' + id_trim +
                                    '" class="badge badge-pill badge-primary">Descargar Artes originales<i class="fas fa-download"></i></a> <div> ';
                                bloque_contenedor.appendChild(A_insertar);
                            } else {
                                /* si la ruta es negativa */
                                A_insertar = document.createElement('div');
                                A_insertar.innerHTML = /* se crea el item con sus dos botones dependiendo del rol pero sin boton para descargar */
                                    '<div class="m-2"><a href="' + url +
                                    '" target="blank" " class="badge badge-pill badge-success">' + textoBoton + '<i class="fas fa-upload"></i></a>' +
                                    descripcion + '<div> ';
                                bloque_contenedor.appendChild(A_insertar);
                            }
                            
                        })

                    "@endif"
                }
            hideLoading()
            });
        }
    

</script>