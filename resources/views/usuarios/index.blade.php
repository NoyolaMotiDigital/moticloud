@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
    <style>
 .input-custom{
    background-color:#212529 !important
 }
</style>
@stop

@section('content')
<a href="/register" class="btn btn-primary mt-3 mb-2">Crear nuevo usuario</a>
<table id="Cotizaciones" class="table table-dark table-hover">
    <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Nombre</th>
            <th scope="col">E-mail</th>
            <th scope="col">contraseña</th>
            <th scope="col">Privilegios</th>
            <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($usuarios as $usuario)
        <tr>
            <td>{{$usuario->id}}</td>
            <form action="/users/{{$usuario->id}}" method="POST">
            @csrf
            @method('PUT')
            <td><input class="bg-dark border-0 input-custom"  value="{{$usuario->name}}" id="usuario_name" name="usuario_name"></td>
            <td><input class="bg-dark border-0 input-custom" value="{{$usuario->email}}" id="usuario_email" name="usuario_email"></td>
            <td><input class="bg-dark border-0 "type="text" id="usuario_pass" name="usuario_pass"></td>
            <td>
            <?php $rol = ["Preprensista","Comercial","Planer","Admin-dev","Admin-preprensa","Admin-comercial"];?>
            <select name="rol" id="rol" class="bg-dark border-0 input-custom">
                    @foreach ($rol as $id => $rol)
                        @if ($usuario->rol == $rol)
                        <option value="{{$rol}}" selected>{{$rol}}</option>
                        @else
                        <option value="{{$rol}}">{{$rol}}</option>
                        @endif
                    @endforeach
                </select>
            </td>
            <td><button type="submit" class="btn btn-info" tabindex="4" onclick="return confirm('estas seguro que quieres editar el usuario?');">guardar cambios</button></td>
            </form>
        </tr>
        @endforeach
    </tbody>
</table>
@stop

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap5.min.css">
@stop

@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap5.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#Cotizaciones').DataTable();
    });
    </script>
@stop