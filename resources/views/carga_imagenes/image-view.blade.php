<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cargar imagenes</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.min.css" media="all"
        rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all"
        rel="stylesheet" type="text/css">
    <style type="text/css">
    .details {
        font-size: 0.78rem !important;
    }

    .page-wrap {
        background-color: #f9f9f9;
        background-image: linear-gradient(#d7d7d7 1px, transparent 1px), linear-gradient(to right, #d7d7d7 1px, #f9f9f9 1px);
        background-size: 20px 20px;
    }

    .gallery {
        display: flex;
        flex-wrap: wrap;
        /* Compensate for excess margin on outer gallery flex items */
        margin: -1rem -1rem;
    }

    .gallery-item {
        /* Minimum width of 24rem and grow to fit available space */
        flex: 1 0 24rem;
        /* Margin value should be half of grid-gap value as margins on flex items don't collapse */
        margin: 1rem;
        overflow: hidden;

        border-radius: 1.2rem !important;
    }

    .gallery-image {
        display: block;
        width: 100%;
        height: 100%;
        object-fit: cover;
        transition: transform 400ms ease-out;
    }

    .gallery-image:hover {
        transform: scale(1.15);
    }

    /*



    */

    @supports (display: grid) {
        .gallery {
            display: grid;
            grid-template-columns: repeat(auto-fit, minmax(14rem, 1fr));
            grid-gap: 2rem;
        }

        .gallery,
        .gallery-item {
            margin: 0;
        }
    }

    .main-section {
        margin: 0 auto;
        padding: 20px;
        margin-top: 100px;
        background-color: #fff;
        box-shadow: 0 0 20px #666666;
        border-radius: 1rem;
    }

    .file-drop-zone {
        border: none !important;
    }

    .btn-secondary {
        color: #626262;
        background-color: #ffffff;
        border-color: #d3cccc;
    }

    .file-error-message {
        display: none;
    }

    .file-preview {
        border: none !important;
    }

    .titulo-cargar {

        font-family: 'Helvetica Neue', sans-serif;
        letter-spacing: -1px;
        text-align: center;
        color: transparent;
        background: #666666;
        -webkit-background-clip: text;
        -moz-background-clip: text;
        background-clip: text;
        text-shadow: 0px 3px 3px rgb(255 255 255 / 50%);
    }
    .subtitulo-cargar {

font-family: 'Helvetica Neue', sans-serif;
color: transparent;
background: #4a4a4a;
-webkit-background-clip: text;
-moz-background-clip: text;
background-clip: text;
text-shadow: 0px 3px 3px rgb(255 255 255 / 50%);
}
.titulo-tarjeta {
    font-family: 'Helvetica Neue', sans-serif;
    color: transparent;
    background: #5c5c5c;
    -webkit-background-clip: text;
    -moz-background-clip: text;
    background-clip: text;
    text-shadow: 0px 3px 3px rgb(255 255 255 / 50%);
    border: solid 2px white;
    border-radius: 0.4rem;
    padding: 0.7rem;
    margin-bottom: 1rem;
    box-shadow: rgb(60 64 67 / 30%) 0 1px 3px 0, rgb(60 64 67 / 15%) 0 4px 8px 3px;
}

    .text-warning {
        display: none !important;
    }

    .btn-custom {
        background-color: #ffffff;
        color: #626262;
        cursor: pointer;
        border: 1px solid #d3cccc;
    }

    .btn-secondary:hover {
        color: #fff;
        background-color: #b9b9b9;
        border-color: #d3cccc;
    }

    .btn-custom:hover {
        background-color: #b9b9b9;
        color: #f7f7f7;
    }

    .file-preview .fileinput-remove {
        font-size: 2.8rem;
    }

    .file-footer-caption {
        color: #5161ce;
    }

    .krajee-default.file-preview-frame {
        box-shadow: 0.5px 0.5px 12px 0 #5161ce40;
        border-radius: 10px;
    }

    .krajee-default.file-preview-frame:not(.file-preview-error):hover {
        box-shadow: 3px 3px 5px 0 #979797;
    }

    .icon-modal {
        font-size: 1.2rem;
        color: #ff9900;
    }

    .modal-content {
        border-radius: 1rem;
    }

    .p-modal {
        padding: 20px 40px;
    }

    .listo {
        text-align: center;
        font-size: 1.1rem;
        padding: 0px 20px;
    }

    .titulo-tarjeta {
        word-break: break-word;
    }

    /* CSS */
    .button-17 {
        align-items: center;
        appearance: none;
        background-color: #fff;
        border-radius: 24px;
        border-style: none;
        box-shadow: rgba(0, 0, 0, .2) 0 3px 5px -1px, rgba(0, 0, 0, .14) 0 6px 10px 0, rgba(0, 0, 0, .12) 0 1px 18px 0;
        box-sizing: border-box;
        color: #3c4043;
        cursor: pointer;
        display: inline-flex;
        fill: currentcolor;
        font-family: "Google Sans", Roboto, Arial, sans-serif;
        font-size: 14px;
        font-weight: 500;
        height: 48px;
        justify-content: center;
        letter-spacing: .25px;
        line-height: normal;
        max-width: 100%;
        overflow: visible;
        padding: 2px 24px;
        position: relative;
        text-align: center;
        text-transform: none;
        transition: box-shadow 280ms cubic-bezier(.4, 0, .2, 1), opacity 15ms linear 30ms, transform 270ms cubic-bezier(0, 0, .2, 1) 0ms;
        user-select: none;
        -webkit-user-select: none;
        touch-action: manipulation;
        width: auto;
        will-change: transform, opacity;
        z-index: 0;
    }

    .button-17:hover {
        background: #F6F9FE;
        color: #174ea6;
    }

    .button-17:active {
        box-shadow: 0 4px 4px 0 rgb(60 64 67 / 30%), 0 8px 12px 6px rgb(60 64 67 / 15%);
        outline: none;
    }

    .button-17:focus {
        outline: none;
        border: 2px solid #4285f4;
    }

    .button-17:not(:disabled) {
        box-shadow: rgba(60, 64, 67, .3) 0 1px 3px 0, rgba(60, 64, 67, .15) 0 4px 8px 3px;
    }

    .button-17:not(:disabled):hover {
        box-shadow: rgba(60, 64, 67, .3) 0 2px 3px 0, rgba(60, 64, 67, .15) 0 6px 10px 4px;
    }

    .button-17:not(:disabled):focus {
        box-shadow: rgba(60, 64, 67, .3) 0 1px 3px 0, rgba(60, 64, 67, .15) 0 4px 8px 3px;
    }

    .button-17:not(:disabled):active {
        box-shadow: rgba(60, 64, 67, .3) 0 4px 4px 0, rgba(60, 64, 67, .15) 0 8px 12px 6px;
    }

    .button-17:disabled {
        box-shadow: rgba(60, 64, 67, .3) 0 1px 3px 0, rgba(60, 64, 67, .15) 0 4px 8px 3px;
    }

    a:hover {
        text-decoration: none !important;
    }

    .button-18 {
        align-items: center;
        appearance: none;
        background-color: #dd5b5b;
        border-radius: 24px;
        border-style: none;
        box-shadow: rgba(0, 0, 0, .2) 0 3px 5px -1px, rgba(0, 0, 0, .14) 0 6px 10px 0, rgba(0, 0, 0, .12) 0 1px 18px 0;
        box-sizing: border-box;
        color: #fff;
        cursor: pointer;
        display: inline-flex;
        fill: currentcolor;
        font-family: "Google Sans", Roboto, Arial, sans-serif;
        font-size: 14px;
        font-weight: 500;
        height: 48px;
        justify-content: center;
        letter-spacing: .25px;
        line-height: normal;
        max-width: 100%;
        overflow: visible;
        padding: 2px 24px;
        position: relative;
        text-align: center;
        text-transform: none;
        transition: box-shadow 280ms cubic-bezier(.4, 0, .2, 1), opacity 15ms linear 30ms, transform 270ms cubic-bezier(0, 0, .2, 1) 0ms;
        user-select: none;
        -webkit-user-select: none;
        touch-action: manipulation;
        width: auto;
        will-change: transform, opacity;
        z-index: 0;
    }

    .button-18:hover {
        background: #f72f2f;
        color: #ffffff;
    }

    .button-18:active {
        box-shadow: 0 4px 4px 0 rgb(60 64 67 / 30%), 0 8px 12px 6px rgb(60 64 67 / 15%);
        outline: none;
    }

    .button-18:focus {
        outline: none;
        border: 2px solid #ff0000;
    }

    .button-18:not(:disabled) {
        box-shadow: rgba(60, 64, 67, .3) 0 1px 3px 0, rgba(60, 64, 67, .15) 0 4px 8px 3px;
    }

    .button-18:not(:disabled):hover {
        box-shadow: rgba(60, 64, 67, .3) 0 2px 3px 0, rgba(60, 64, 67, .15) 0 6px 10px 4px;
    }

    .button-18:not(:disabled):focus {
        box-shadow: rgba(60, 64, 67, .3) 0 1px 3px 0, rgba(60, 64, 67, .15) 0 4px 8px 3px;
    }

    .button-18:not(:disabled):active {
        box-shadow: rgba(60, 64, 67, .3) 0 4px 4px 0, rgba(60, 64, 67, .15) 0 8px 12px 6px;
    }

    .button-18:disabled {
        box-shadow: rgba(60, 64, 67, .3) 0 1px 3px 0, rgba(60, 64, 67, .15) 0 4px 8px 3px;
    }
    </style>
</head>

<body lang="es">
    <div class="page-wrap">
        <div class="pos-f-t">
            <div class="collapse" id="navbarToggleExternalContent">
                <div class="bg-dark p-4">
                    <h4 class="text-white">Subir imagenes</h4>
                    <span class="text-muted">Todas las imagenes que subas en esta ventana se subirán a la carpeta de <span style="color:#ffc107!important;">{{$id}}</span></span>
                </div>
            </div>
            <nav class="navbar navbar-dark bg-dark">
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </nav>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg col-sm-12 col-11 main-section">
                    <h1 class="titulo-cargar">Subir imagenes a ruta</h1>
                    <hr>
                    <h3 class="titulo-cargar">Carpeta: {{$id}}</h3>
                    <input type="hidden" name="numeroId" value="{{$id}}" style="display: none;">
                    <input type="hiden" name="_token" value="{{csrf_token()}}" style="display: none;">

                    <div class="form-group">
                        <input type="file" id="file-1" name="file" multiple class="file" data-overwrite-initial="false"
                            data-min-file-count="1">
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg col-sm-12 col-11 main-section">
                            <h1 class="titulo-cargar">Imagenes en la carpeta actualmente</h1>
                            <hr>
                            <form action="{{route ('Image.destroy', $id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="button-18 mb-3">Borrar todos los archivos</button>
                            </form>
                            <div class="gallery">
                                <?php
                                /*  considerar mejorar urgente  */
                                $Orden = explode("-",$id);
                                $Orden = preg_replace('/\s+/', '', $Orden);
                                if ($Orden[0]=='Archivo'){
                                    $micarpeta = "storage/Puebas_desarrollo_hybrid_2.0/$Orden[0]/Artes-$Orden[2]/Archivos-$Orden[2]-$Orden[3]";
                                }
                                else{
                                    $micarpeta = "storage/Puebas_desarrollo_hybrid_2.0/$Orden[0]/Artes-$Orden[2]/Artes-$Orden[2]-item$Orden[3]/$Orden[1]-$Orden[2]-$Orden[3]";
                                }
                                if (is_dir($micarpeta)){
                                    // Abre un gestor de directorios para la micarpeta indicada
                                    $gestor = opendir($micarpeta);
                                    // Recorre todos los archivos del directorio
                                    while (($archivo = readdir($gestor)) !== false)  {
                                        // Solo buscamos archivos sin entrar en subdirectorios
                                        if (is_file($micarpeta."/".$archivo)) {
                                            if(@is_array(getimagesize($micarpeta."/".$archivo))){
                                            ?>
                                <div>
                                    <div class="gallery-item">
                                        <img class="gallery-image" src="{{ url($micarpeta.'/'.$archivo) }}"
                                            width='200px' alt="{{$archivo}}" title="true" />
                                    </div>
                                    <div class="card-body">
                                        <h6 class="titulo-tarjeta mb-2">{{$archivo}}</h5>
                                            <p class="subtitulo-cargar mb-0 details"><b>Extensión:
                                                </b>{{pathinfo($archivo, PATHINFO_EXTENSION);}}</p>
                                            <p class="subtitulo-cargar mb-0 details"><b>Tamaño de archivo:
                                                </b>{{filesize($micarpeta.'/'.$archivo)}}</p>
                                            <p class="subtitulo-cargar mb-0 details"><b>Fecha de creación:
                                                </b>{{ date ("d/m/y h:m", filemtime($micarpeta.'/'.$archivo));}}</p>
                                            <a href="/download/imagen/{{$id}}/{{$archivo}}"
                                                class="button-17 m-3">Descargar
                                                archivo</a>
                                            <a href="/delete-item/{{$id}}/{{$archivo}}" class="button-18 m-3">Eliminar
                                                archivo</a>
                                    </div>
                                </div>

                                <?php
                                            } 
                                            else {
                                                switch (pathinfo($archivo, PATHINFO_EXTENSION)){
                                                    case "mp4": {
                                                        ?>
                                <div>
                                    <div class="gallery-item">
                                        <video width="100%" height="auto" controls>
                                            <source src="{{ url($micarpeta.'/'.$archivo) }}" type="video/mp4">intenta
                                            con otro navegador.
                                        </video>
                                    </div>
                                    <div class="card-body">
                                        <h6 class="titulo-tarjeta mb-2">{{$archivo}}</h5>
                                            <p class="subtitulo-cargar mb-0 details"><b>Extensión:
                                                </b>{{pathinfo($archivo, PATHINFO_EXTENSION);}}</p>
                                            <p class="subtitulo-cargar mb-0 details"><b>Tamaño de archivo:
                                                </b>{{filesize($micarpeta.'/'.$archivo)}}</p>
                                            <p class="subtitulo-cargar mb-0 details"><b>Fecha de creación:
                                                </b>{{ date ("d/m/y h:m", filemtime($micarpeta.'/'.$archivo));}}</p>
                                            <a href="/download/imagen/{{$id}}/{{$archivo}}"
                                                class="button-17 m-3">Descargar
                                                archivo</a>
                                            <a href="/delete-item/{{$id}}/{{$archivo}}" class="button-18 m-3">Eliminar
                                                archivo</a>
                                    </div>
                                </div>
                                <?php
                                                        break;
                                                    }
                                                    case "pdf":{
                                                        ?>
                                <div>
                                    <div class="gallery-item">
                                        <embed src="{{ url($micarpeta.'/'.$archivo) }}" type="application/pdf"
                                            frameBorder="0" scrolling="auto" height="100%" width="100%"></embed>
                                    </div>
                                    <div class="card-body">
                                        <h6 class="titulo-tarjeta mb-2">{{$archivo}}</h5>
                                            <p class="subtitulo-cargar mb-0 details"><b>Extensión:
                                                </b>{{pathinfo($archivo, PATHINFO_EXTENSION);}}</p>
                                            <p class="subtitulo-cargar mb-0 details"><b>Tamaño de archivo:
                                                </b>{{filesize($micarpeta.'/'.$archivo)}}</p>
                                            <p class="subtitulo-cargar mb-0 details"><b>Fecha de creación:
                                                </b>{{ date ("d/m/y h:m", filemtime($micarpeta.'/'.$archivo));}}</p>
                                            <a href="/download/imagen/{{$id}}/{{$archivo}}"
                                                class="button-17 m-3">Descargar
                                                archivo</a>
                                            <a href="/delete-item/{{$id}}/{{$archivo}}" class="button-18 m-3">Eliminar
                                                archivo</a>
                                    </div>
                                </div>
                                <?php
                                                        break;
                                                    }
                                                    case "xlsx":{
                                                        ?>
                                <div>
                                    <div class="gallery-item" style="height: 13rem;background: #efefef;">
                                        <h5 class="text-center"><span class="badge bg-secondary ">Sin vista
                                                previa</span></h5>
                                    </div>
                                    <div class="card-body">
                                        <h6 class="titulo-tarjeta mb-2">{{$archivo}}</h5>
                                            <p class="subtitulo-cargar mb-0 details"><b>Extensión:
                                                </b>{{pathinfo($archivo, PATHINFO_EXTENSION);}}</p>
                                            <p class="subtitulo-cargar mb-0 details"><b>Tamaño de archivo:
                                                </b>{{filesize($micarpeta.'/'.$archivo)}}</p>
                                            <p class="subtitulo-cargar mb-0 details"><b>Fecha de creación:
                                                </b>{{ date ("d/m/y h:m", filemtime($micarpeta.'/'.$archivo));}}</p>
                                            <a href="/download/imagen/{{$id}}/{{$archivo}}"
                                                class="button-17 m-3">Descargar
                                                archivo</a>
                                            <a href="/delete-item/{{$id}}/{{$archivo}}" class="button-18 m-3">Eliminar
                                                archivo</a>
                                    </div>
                                </div>
                                <?php
                                                        break;
                                                    }
                                                }
                                                
                                            }
                                        }            
                                    }
                                    // Cierra el gestor de directorios
                                    closedir($gestor);
                                } else {
                                    echo "No se encontró la carpeta<br/>";
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body"><button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="titulo-cargar">Revisemos todo una vez más</h2>
                    <p class="subtitulo-cargar"><br>
                        <i class="fa fa-check-square icon-modal" aria-hidden="true"></i> Revisa el
                        nombre del archivo <br>
                        <i class="fa fa-check-square icon-modal" aria-hidden="true"></i> El tamaño del
                        archivo es el correcto?<br>
                        <i class="fa fa-check-square icon-modal" aria-hidden="true"></i> Es el archivo
                        correcto? (ver preview)<br><br>

                    </p>
                    <div class="listo">
                        <span class="subtitulo-cargar">Está todo correcto? da click en "subir a micarpeta"
                            para terminar el proceso.
                        </span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js"
    type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.2.5/js/locales/es.js"></script>
<script type="text/javascript">
$("#file-1").fileinput({
    theme: 'fa',
    /* micarpeta que hace referencia a ImageController en la clase store, ver web.php  */
    uploadUrl: "/image-submit",
    uploadExtraData: function() {
        return {
            _token: $("input[name='_token']").val(),
            _id: $("input[name='numeroId']").val()
        };
    },
    /* descomentar esta linea si se requiere que limiten que tipo de archivo subimos al server*/
    /*  allowedFileExtensions: ["jpg", "png", "ai"], */
    overwriteInitial: false,
    maxFileSize: 60000000,
    maxFileSizeNum: 60000,
    language: 'es',
    /* no borrar esta linea que dará error al subir archivos pesados ya que los divide el tiempo y recurso de la memoria  */
    enctype: 'multipart/form-data',
    /* se agregaron las traducciones de manera local ya que anteriormente se han pedido cambios muy especificos y para hacerlo de manera mas sencilla  */
    removeLabel: 'Limpiar pantalla',
    uploadLabel: 'subir a micarpeta',
    /* removeFromPreviewOnError: true,
    overwriteInitial: true, */
    /* overwrite: true, */
    browseClass: "btn btn-custom",
    /* uploadAsync: false, */
}).on("filebatchselected", function() {
    $('#myModal').modal('show');

});
</script>

</html>