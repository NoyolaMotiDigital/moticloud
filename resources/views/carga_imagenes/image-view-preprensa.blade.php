<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cargar imagenes</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.min.css" media="all"
        rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all"
        rel="stylesheet" type="text/css">
    <style type="text/css">
    .main-section {
        margin: 0 auto;
        padding: 20px;
        margin-top: 100px;
        background-color: #fff;
        box-shadow: 0 0 20px #c1c1c1;
    }

    .file-drop-zone {
        border: none !important;
    }

    .file-error-message {
        display: none;
    }

    .file-preview {
        border: none !important;
    }

    .titulo-cargar {
        font-size: 2.5rem;
        background-color: #495057;
        text-align: center;
        padding: 5px 0px;
        color: white;
        border-radius: 8px;
        box-shadow: 4px 4px 4px 1px rgba(0, 0, 0, 0.2);
    }

    .text-warning {
        display: none !important;
    }

    .btn-custom {
        background-color: #5161ce;
        color: white;
        cursor: pointer;
    }

    .btn-custom:hover {
        background-color: #354087;
        color: #b5b5b5;
    }

    .file-preview .fileinput-remove {
        font-size: 2.8rem;
    }

    .btn-outline-secondary {
        color: #5161ce;
        border-color: #5161ce;
        border-radius: 100% !important;
    }

    .btn-outline-secondary:hover {
        color: #fff;
        background-color: #5161ce;
        border-color: #5161ce;
    }

    .file-footer-caption {
        color: #5161ce;
    }

    .krajee-default.file-preview-frame {
        box-shadow: 0.5px 0.5px 12px 0 #5161ce40;
        border-radius: 10px;
    }

    .krajee-default.file-preview-frame:not(.file-preview-error):hover {
        box-shadow: 3px 3px 5px 0 #979797;
    }

    .titulo-modal {
        font-size: 2rem;
        color: #5161ce;
        text-align: center;
        padding: 20px;
    }

    .icon-modal {
        font-size: 1.2rem;
        color: #ff7600;
    }

    .modal-content {
        border-radius: 1rem;
    }

    .p-modal {
        padding: 20px 40px;
    }

    .listo {
        text-align: center;
        font-size: 1.1rem;
        padding: 0px 20px;
    }
    </style>
</head>

<body lang="es">
    <div class="container">
        <div class="row">
            <div class="col-lg col-sm-12 col-11 main-section">
                <h1 class="titulo-cargar">Subir imagenes a ruta(desing)</h1>
                <h3 >numero de orden : {{$id}}</h3>
                <input type="hidden" name="numeroId" value="{{$id}}"style="display: none;">
                <i class="fa fa-info-circle" aria-hidden="true" style="color: #8c95d7;font-size: 2rem;"
                    data-toggle="tooltip-subir" title="Las imagenes se subiran a la ruta de preprensa"></i>
                <input type="hiden" name="_token" value="{{csrf_token()}}" style="display: none;">
                <div class="form-group">
                    <input type="file" id="file-1" name="file" multiple class="file" data-overwrite-initial="false"
                        data-min-file-count="1">
                </div>
            </div>
            
        </div>
    </div>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body"><button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span class="titulo-modal">Revisemos todo una vez más</span>
                    <p class="p-modal"><br>
                        <i class="fa fa-check-square icon-modal" aria-hidden="true"></i> Revisa el
                        nombre del archivo <br>
                        <i class="fa fa-check-square icon-modal" aria-hidden="true"></i> El tamaño del
                        archivo es el correcto?<br>
                        <i class="fa fa-check-square icon-modal" aria-hidden="true"></i> Es el archivo
                        correcto? (ver preview)<br><br>

                    </p>
                    <div class="listo">
                        <span>Está todo correcto? da click en "subir a ruta"
                            para terminar el proceso.
                        </span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js"
    type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.2.5/js/locales/es.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('[data-toggle="tooltip-subir"]').tooltip();
});

$("#file-1").fileinput({
    theme: 'fa',
    /* ruta que hace referencia a ImageController en la clase store, ver web.php  */
    uploadUrl: "/image-submit-preprensa",
    uploadExtraData: function() {
        return {
            _token: $("input[name='_token']").val(),
            _id: $("input[name='numeroId']").val()
        };
    },
    /* descomentar esta linea si se requiere que limiten que tipo de archivo subimos al server*/
    /*  allowedFileExtensions: ["jpg", "png", "ai"], */
    overwriteInitial: false,
    maxFileSize: 60000000,
    maxFileSizeNum: 60000,
    language: 'es',
    /* no borrar esta linea que dará error al subir archivos pesados ya que los divide el tiempo y recurso de la memoria  */
    enctype: 'multipart/form-data',
    /* se agregaron las traducciones de manera local ya que anteriormente se han pedido cambios muy especificos y para hacerlo de manera mas sencilla  */
    removeLabel: 'Limpiar pantalla',
    uploadLabel: 'subir a ruta',
    /* removeFromPreviewOnError: true,
    overwriteInitial: true, */
    /* overwrite: true, */
    browseClass: "btn btn-custom",
    /* uploadAsync: false, */
}).on("filebatchselected", function() {
    $('#myModal').modal('show');

});
</script>

</html>