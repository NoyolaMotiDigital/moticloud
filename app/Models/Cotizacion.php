<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cotizacion extends Model
{
    use HasFactory;
    public function user() {
        return $this->belongsTo(User::class,'user_id'); // eloquent interpretará que la fk es el nombre de la clase en snakecase seguido de _id, o sea 'user_id' 
    }
    public function creator_user() {
        return $this->belongsTo(User::class,'creador'); // eloquent interpretará que la fk es el nombre de la clase en snakecase seguido de _id, o sea 'user_id' 
    }
}
