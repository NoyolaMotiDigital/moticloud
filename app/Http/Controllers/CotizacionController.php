<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cotizacion;
use App\Models\User;
use Illuminate\Support\ViewErrorBag;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class CotizacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->rol == 'Admin-comercial' || Auth::user()->rol == 'Admin-preprensa') {
            $cotizacions = Cotizacion::where('estatus', 'Preprensa')->get();
        } elseif (Auth::user()->rol == 'Preprensista') {
            $cotizacions = Cotizacion::where('estatus', 'Preprensa')->where('user_id', Auth::user()->id)->get();
        } elseif (Auth::user()->rol == 'Planer') {
            $cotizacions = Cotizacion::where('estatus', 'Listo para impresion')->get();
        } elseif (Auth::user()->rol == 'Comercial') {
            $cotizacions = Cotizacion::where('estatus', 'Preprensa')->get();
        } elseif (Auth::user()->rol == 'Admin-dev') {
            $cotizacions = Cotizacion::all();
        } else {
            return view('errors.404');
        }
        return view('cotizacion.index')->with('cotizacions', $cotizacions);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->rol == 'Admin-comercial' || Auth::user()->rol == 'Comercial' || Auth::user()->rol == 'Admin-dev') {
            return view('cotizacion.create');
        } else {
            return view('errors.404');
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->rol == 'Admin-comercial' || Auth::user()->rol == 'Comercial' || Auth::user()->rol == 'Admin-dev') {
            $validacion = Cotizacion:://where('no_cotizacion', $request->get('no_cotizacion')->whereNotIn("estatus", "Cancelado"))->first();
            where([['no_cotizacion', '=', $request->get('no_cotizacion')],['estatus', '<>', 'Cancelado']])->first();
            if ($validacion == null) {

                $cotizacion = new Cotizacion();
                $cotizacion->creador = Auth::user()->id;
                $cotizacion->no_cotizacion = $request->get('no_cotizacion');
                $cotizacion->no_trabajo = $request->get('no_trabajo');
                $cotizacion->estatus = 'Preprensa';
                $preprensista = User::where('rol', 'Admin-preprensa')->first();
                $cotizacion->user_id = $preprensista->id;
                $cotizacion->fecha_produccion = $request->get('fecha_produccion');
                $cotizacion->fecha_vencimiento = $request->get('fecha_vencimiento');
                $cotizacion->fecha_preprensa = $request->get('fecha_preprensa');
                $cotizacion->comentarios = $request->get('comentarios');
                $response = Http::post('http://motidigitalsupport.ddns.net:8082/moti-api-v2/public/api/quotes/'.$request->get('no_cotizacion').'/job/preprensa', [
                    'jobProject' => 5023,
                    'routingTemplate' => 6,
                    'priority' => 7,
                    'quoteDueDate' => $request->get('fecha_produccion').'T06:00:00.000Z',
                    'earliestStartDate' => $request->get('fecha_vencimiento').'T06:00:00.000Z',
                    'promiseDate' => $request->get('fecha_vencimiento').'T06:00:00.000Z',
                ]);
                if (json_decode($response->body())->ok == true){
                    $cotizacion->no_trabajo = json_decode($response->body())->productionOrder;
                    $orden_produccion = json_decode($response->body())->productionOrder;
                    $data = Http::get('http://motidigitalsupport.ddns.net:8082/moti-api-v2/public/api/jobs/'.$orden_produccion);
                    $data = json_decode($data);
                    $data->data->adminStatus ="F";
                    if ($data->ok == true)
                    {
                        $json = json_encode($data->data);
                        $response = Http::withHeaders(['Content-Type' => 'application/json'])
                           ->send('POST', 'http://motidigitalsupport.ddns.net:8082/moti-api-v2/public/api/jobs/'.$orden_produccion, [
                               'body' => $json
                           ])->json();
                           if ($response['ok'] != true){
                            return redirect()->back()->with('error', '(error cambio estado cloudflow) no se pudo crear la orden :'. $request->get('no_cotizacion'));
                           }
                           else{
                            $cotizacion->save();
                           } 
                    }
                }
                else {
                    return redirect()->back()->with('error', '(error crear el trabajo en EFI) no se pudo crear la orden :'. $request->get('no_cotizacion'));
                }
                    
            } else {

                return redirect()->back()->with('error', 'Ya existe una cotización con este numero :'. $request->get('no_cotizacion'));
            }
        } else {
            return view('errors.404');
        }
        return redirect('cotizacions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cotizacion = Cotizacion::find($id);
        $usuarios = User::all();
        return view('cotizacion.edit')->with('cotizacion', $cotizacion)->with('usuarios', $usuarios);
    }

    public function status($ruta, $id)
    {
        $cotizacion = Cotizacion::find($id);
        $cotizacion->estatus = $ruta;
        $response = Http::get('http://motidigitalsupport.ddns.net:8082/moti-api-v2/public/api/quotes/'.$cotizacion->no_cotizacion);
        $orden_produccion =json_decode($response->body())->response->jobConvertedTo;
        $data = Http::get('http://motidigitalsupport.ddns.net:8082/moti-api-v2/public/api/jobs/'.$orden_produccion);
        $data = json_decode($data);
        $data->data->adminStatus ="O";
        if ($data->ok == true)
        {
            $json = json_encode($data->data);
            $response = Http::withHeaders(['Content-Type' => 'application/json'])
            ->send('POST', 'http://motidigitalsupport.ddns.net:8082/moti-api-v2/public/api/jobs/'.$orden_produccion, [
                'body' => $json
                ])->json();
                if ($response['ok'] != true){
                    return redirect()->back()->with('error', '(error cambio estado cloudflow) no se pudo cerrar la orden :'.$cotizacion->no_cotizacion);
                }
                else{
                    $cotizacion->save();
                    return redirect('cotizacions');
                } 
        }
    }


    /*
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cotizacion = Cotizacion::find($id);
        $cotizacion->no_cotizacion = $request->get('no_cotizacion');
        $cotizacion->no_trabajo = $request->get('no_trabajo');
        if ($request->empleado != null) {
            $cotizacion->user_id = $request->empleado;
        }
        $cotizacion->comentarios = $request->get('comentarios');
        $cotizacion->save();
        return redirect('cotizacions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cotizacion = Cotizacion::find($id);
        $cotizacion->estatus = 'Cancelado';
        $cotizacion->save();

        return redirect('cotizacions');
    }
}