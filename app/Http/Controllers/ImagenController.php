<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ZipArchive;
use File;
use Illuminate\Support\Facades\Http;

/* tener en mente futuras mejoras 
1. hacer una funcion que haga lo siguiente :
$Orden = explode("-",$request->get('_id'));
$Orden = preg_replace('/\s+/', '', $Orden);
$micarpeta = "storage/Puebas_desarrollo_hybrid_2.0/Artes-$Orden[1]/originales-$Orden[1]-$Orden[2]";

2. posibilidad de usar una variable global */

class ImagenController extends Controller
{
    public function index($id){
        return view('carga_imagenes.image-view')->with('id', $id);
        /* $numOrden = $id;
        $micarpeta = "storage/Puebas_desarrollo_hybrid_2.0/Artes-$numOrden/originales-$numOrden";
        if (!file_exists($micarpeta)) {
            mkdir($micarpeta, true);
        } */
    }
    public function store(Request $request){
        $Orden = explode("-",$request->get('_id'));
        $Orden = preg_replace('/\s+/', '', $Orden);
        /* ruta a la que iran los archivos */
        $Idcliente = $this->getClientId($Orden[2]);
        if ($Orden[0]=='Archivo'){
            $micarpeta = "storage/Puebas_desarrollo_hybrid_2.0/$Orden[0]/Artes-$Orden[2]/Archivos-$Orden[2]-$Orden[3]";
        }
        else{
            $micarpeta = "storage/Puebas_desarrollo_hybrid_2.0/$Orden[0]/Artes-$Orden[2]/Artes-$Orden[2]-item$Orden[3]/$Orden[1]-$Orden[2]-$Orden[3]";
        }
        /* dd(file_exists($micarpeta)); */
        
        $imageName = $request->file->getClientOriginalName();
         /* se mueven los archivos */
        $request->file->move($micarpeta,$imageName);
        return response()->json (['uploaded'=>'/upload/'.$imageName]);
    }
    public function destroy($carpeta){
        $Orden = explode("-",$carpeta);
        $Orden = preg_replace('/\s+/', '', $Orden);
        /* ruta a la que iran los archivos */
        $micarpeta = "storage/Puebas_desarrollo_hybrid_2.0/$Orden[0]/Artes-$Orden[2]/Artes-$Orden[2]-item$Orden[3]/$Orden[1]-$Orden[2]-$Orden[3]/*";
        $files = glob($micarpeta); // obtiene los nombres
            foreach($files as $file){ // itera archivos
                    if(is_file($file)) {
                        unlink($file); // borra archivo
                     }
            }
            return redirect('/cotizacions/imagen/'.$carpeta);
    } 
    public function download($carpeta){
        $Orden = explode("-",$carpeta);
        $Orden = preg_replace('/\s+/', '', $Orden);
        /* dd($Orden); */
        /* ruta a la que iran los archivos */
        $micarpeta = "storage/Puebas_desarrollo_hybrid_2.0/$Orden[0]/Artes-$Orden[2]/Artes-$Orden[2]-item$Orden[3]/$Orden[1]-$Orden[2]-$Orden[3]";
        $zip = new ZipArchive;
        $nombreArchivo = "$Orden[0]-$Orden[1]-$Orden[2].zip";
        if ($zip->open(public_path($nombreArchivo),ZipArchive::CREATE)=== TRUE){
            $files = File::files(public_path($micarpeta));
            foreach ($files as $key => $value){
                $nombreRelativo= basename($value);
                $zip->addFile($value,$nombreRelativo);
            }
            $zip->close();
        }
            return response()->download(public_path($nombreArchivo))->deleteFileAfterSend(true);

    }
     public function downloadId ($carpeta, $archivo)  {
        $Orden = explode("-",$carpeta);
        $Orden = preg_replace('/\s+/', '', $Orden);
        
        /* ruta a la que iran los archivos */
        $micarpeta = "storage/Puebas_desarrollo_hybrid_2.0/$Orden[0]/Artes-$Orden[2]/Artes-$Orden[2]-item$Orden[3]/$Orden[1]-$Orden[2]-$Orden[3]/$archivo";
        /* crea variable para descarga */
        return response()->download(public_path($micarpeta));
    } 
    public function destroyId($carpeta, $archivo){
        $Orden = explode("-",$carpeta);
        $Orden = preg_replace('/\s+/', '', $Orden);
        /* ruta a la que iran los archivos */
        $micarpeta = "storage/Puebas_desarrollo_hybrid_2.0/$Orden[0]/Artes-$Orden[2]/Artes-$Orden[2]-item$Orden[3]/$Orden[1]-$Orden[2]-$Orden[3]/$archivo";
        unlink($micarpeta);
        return redirect('/cotizacions/imagen/'.$carpeta);
        
    }
    public function getClientId($cotizacion){
        $cotizacion = $cotizacion[1];
        $response = Http::get('http://motidigitalsupport.ddns.net:8082/moti-api-v2/public/api/quotes/'.$cotizacion, [
        ]);
        $response = json_decode($response->body())->response->customer;
        return $response;
    }
}
