<?php

namespace Database\Seeders;

use App\Models\Cotizacion;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => "desarrollo2",
            'email' => "desarrollo2@motidigital.com",
            'password' => Hash::make('moti2021*'),
            'rol' => 'Admin-dev'
        ]);
        User::create([
            'name' => "Priscilla",
            'email' => "pris@moti.com",
            'password' => Hash::make('moti2323'),
            'rol' => 'Comercial'
        ]);
        User::create([
            'name' => "Genesta",
            'email' => "omar@moti.com",
            'password' => Hash::make('moti2323'),
            'rol' => 'Admin-comercial'
        ]);
        User::create([
            'name' => "Lety",
            'email' => "lety@moti.com",
            'password' => Hash::make('moti2323'),
            'rol' => 'Admin-preprensa'
        ]);
        User::create([
            'name' => "charli",
            'email' => "charly@moti.com",
            'password' => Hash::make('moti2323'),
            'rol' => 'Preprensista'
        ]);
        User::create([
            'name' => "castellanos",
            'email' => "castellanos@moti.com",
            'password' => Hash::make('moti2323'),
            'rol' => 'Planer'
        ]);
    }
}
