<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCotizacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cotizacions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('creador')->nulleable()->constrained('users')->cascadeOnUpdate();
            $table->string('no_cotizacion',10);
            $table->string('no_trabajo',10)->nullable();
            $table->enum("estatus",["Preprensa","Listo para impresion","Cerrado","Cancelado"]);
            $table->foreignId('user_id')->nulleable()->constrained('users')->cascadeOnUpdate();
            $table->text('comentarios')->nullable();
            $table->string('fecha_produccion',20);
            $table->string('fecha_vencimiento',20);
            $table->string('fecha_preprensa',20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cotizacions');
    }
}
