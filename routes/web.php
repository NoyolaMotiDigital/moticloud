<?php

use Illuminate\Support\Facades\Route;
use Symfony\Component\Routing\Route as RoutingRoute;
use App\Http\Controllers\ImagenController;
use App\Http\Controllers\CotizacionController;
use Laravel\Fortify\Http\Controllers\RegisteredUserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::middleware(['auth'])->group(function () {
Route:: resource('cotizacions','App\Http\Controllers\CotizacionController');
Route::post('/cambiar-status/{ruta}/{id}', [CotizacionController::class, 'status'])->name('cotizacion.status');

Route:: resource('users','App\Http\Controllers\UserController');
Route:: resource('Image','App\Http\Controllers\ImagenController');
/* rutas para la cargas de imagenes */
Route::get('/cotizacions/imagen/{id}', [ImagenController::class, 'index'])->name('vistaImagenes');
Route::get('/delete-item/{carpeta}/{archivo}', [ImagenController::class, 'destroyId'])->name('imagen.destroyId');
Route::get('/download/imagen/{carpeta}/{archivo}', [ImagenController::class, 'downloadId'])->name('descargaImagenes');
Route::get('/cotizacions/download/imagen/{carpeta}', [ImagenController::class, 'download'])->name('descargaCarpeta');
Route::post('image-submit', [ImagenController::class, 'store'])->name('imagen.store');
/* rutas para registrar user*/
Route::get('/register', [RegisteredUserController::class, 'create'])->name('register');
Route::post('/register', [RegisteredUserController::class, 'store']);
/* rutas para- la cargas de imagenes */
});
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
